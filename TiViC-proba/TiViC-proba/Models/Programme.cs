﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TiViC_proba.Models
{
    public class Programme
    {
        public string kanalId { get; set; }

        [Display(Name="Datum")]
        public string datum { get; set; }

        [Display(Name="Vreme")]
        public string vreme { get; set; }

        [Display(Name = "Naziv")]
        public string naziv { get; set; }

        [Display(Name = "Opis")]
        public string opis { get; set; }

        [Display(Name = "Tip")]
        public string tip { get; set; }
    }
}
