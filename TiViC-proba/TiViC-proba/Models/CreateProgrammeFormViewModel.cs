﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TiViC_proba.Models
{
    public class CreateProgrammeFormViewModel
    {
        public List<Kanal> Kanali { get; set; }
        public Programme Programme { get; set; }
    }
}
