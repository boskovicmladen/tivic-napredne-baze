﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TiViC_proba.Models
{
    public class Programi_po_KanaluViewModel
    {
        public string KanalId { get; set; }
        public string Datum { get; set; }
        public IEnumerable<Kanal> Kanali { get; set; }
        public IEnumerable<Programme> Programi { get; set; }
        
    }
}
